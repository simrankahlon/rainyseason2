
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *

class ValidateAddChangeEmailIdForm(FormValidationAction):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "validate_add_change_email_id_form"

	def validate_new_user_email(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		new_user_email = tracker.get_slot("new_user_email")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"new_user_email",new_user_email)
		return {"new_user_email":new_user_email}

	
	def validate_confirm_user_email(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		confirm_user_email = tracker.get_slot("confirm_user_email")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"confirm_user_email",confirm_user_email)
		return {"confirm_user_email":confirm_user_email}