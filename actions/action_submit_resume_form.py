
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *
from lib.resume import *

class SubmitResumeForm(Action):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "action_submit_resume_form"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
		try:
			#Get the current intent name
			intent_name = tracker.latest_message["intent"]["name"]
			#Get the sender id
			tracker_id = tracker.sender_id
			#Get the value of resume confirmation
			resume_confirmation = get_slot(tracker_id,'resume_confirmation')
			#Get the response message from DB
			response_message = get_slot(tracker_id,'response_message')
			print(response_message)
			#Get the resume intent list from Database
			resume_list_response = getResumableList(sender_id=tracker_id)
			if resume_list_response.get("success") == 1:
				resume_intent_list = resume_list_response.get("data")
			else:
				resume_intent_list = []

			intent_to_resume = ""
			#If resume intent list is not none
			if resume_intent_list:
				intent_to_resume = resume_intent_list[-1]
			#Set resume confirmation as None in DB.
			db_slots = {}
			db_slots["resume_confirmation"] = None
			db_slots["response_message"] = None
			#If the user wishes to resume this intent
			if resume_confirmation == "yes":
				#get the form name to resume
				form_to_resume = intent_to_resume.get('form')
				#Get the clear slot on resume field
				clear_slots_on_resume = intent_to_resume.get("clear_slots_on_resume")
				#Get the intent name to resume
				intent_name_to_resume = intent_to_resume.get('intent_name')
				#Replace the resume list in the db again
				replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
				#On resume start from first slot, clear all the prefilled slots.
				if clear_slots_on_resume == "yes":
					intent_slots = intent_to_resume.get("slots")
					slots = []
					for i in intent_slots:
						slots.append(SlotSet(i,None))
						db_slots[i] = None
						set_multiple_slots(tracker_id,db_slots)
					return [FollowupAction(form_to_resume), SlotSet("resume_confirmation", None)]+slots
				else:
					set_multiple_slots(tracker_id,db_slots)
					return [FollowupAction(form_to_resume), SlotSet("resume_confirmation", None)]
			#If the user doesnt wish to resume
			elif resume_confirmation == "no" or resume_confirmation == "answered":
				print("in the else case of resume")
				#If the user doesnt want to resume, remove that intent from the resume intent list
				if resume_intent_list:
					resume_intent_list.pop()
				
				#get the last intent in the list to resume if its resumable then only
				i = 1
				while len(resume_intent_list) > 0:
					latest_intent_name = resume_intent_list[-i]
					if latest_intent_name.get("resumable") == 'yes':
						break
					else:
						resume_intent_list.pop()
						i = 1

				#Replace the resume list in the db again
				replaceResumableList(sender_id=tracker_id,data=resume_intent_list)
				print("89 list is")
				print(resume_intent_list)
				#If there are other intents to resume
				if resume_intent_list:
					intent_to_resume = resume_intent_list[-1]
					intent_name_to_resume = intent_to_resume.get('intent_name')
					print("the intent to resume is")
					print(intent_name_to_resume)
					db_slots["intent_to_resume"] = intent_name_to_resume
					db_slots["resume_message"] = intent_to_resume.get('resume_message')
					print("in here")
					print(db_slots)
					set_multiple_slots(tracker_id,db_slots)
					return [FollowupAction("resume_form"), SlotSet("resume_confirmation", None)]
				#If there is nothing to resume
				else:
					resume_message = "What else can I help you with?"
					if response_message != None and resume_confirmation == "answered":					
						resume_message = response_message + resume_message
					message = (
						[
							{
								"message": {
									"template": {
										"elements": {
											"title" : resume_message,
										},
										"type": "Card",
									}
								}
							},
							{"ssml": "<speak>"+ resume_message +"</speak>"}
						]
					)
					set_multiple_slots(tracker_id,db_slots)
					dispatcher.utter_message(json.dumps(message))
					return [FollowupAction('action_listen'),SlotSet("resume_confirmation", None)]
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []