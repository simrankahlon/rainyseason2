
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *

class ValidateAskToSwitchForm(FormValidationAction):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "validate_ask_to_switch_form"

	def validate_switch_confirmation(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		switch_confirmation = tracker.get_slot("switch_confirmation")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"switch_confirmation",switch_confirmation)
		return {"switch_confirmation":switch_confirmation}