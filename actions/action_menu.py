
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re

class Menu(Action):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""
        return "action_menu"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
        try:
            intent_name = "menu"
            #Get the tracker id from the tracker
            tracker_id = tracker.sender_id
            #Update the resume list
            message = (
                        [
                            {
                                "message": {
                                    "template": {
                                        "elements": {
                                            "title" : "In menu form",
                                        },
                                        "type": "Card",
                                    }
                                }
                            },
                            {"ssml": "<speak>In menu form</speak>"}
                        ]
                    )
            dispatcher.utter_message(json.dumps(message))
            return []
        except BaseException as e:
            print(e)
            ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
            fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
            dispatcher.utter_message(fallback_message)
            return []