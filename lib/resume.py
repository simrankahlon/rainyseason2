import logging
import json
from lib.db_queries import *
from rasa_sdk.events import SlotSet

def addIntentToResumableList(intent_name,intent_details,resume_intent_list):

	#Get the form name and other details and append it to the resume intent array
	intent_to_resume = {'intent_name': intent_name,
						'title': intent_details.get("title"),
						'form':intent_details.get("action"),
						'resumable': intent_details.get("resumable"),
						'clear_slots_on_resume': intent_details.get("clear_slots_on_resume"),
						'slots': intent_details.get("slots"),
						'resume_message' : intent_details.get("resume_message")
						}
	
	intent_exists = 0
	# Check if intent already exists in the array
	for i in resume_intent_list:
		#check basis on the intent name
		if i.get("intent_name") == intent_name:
			#If it exists assign its current state and values to a variable
			intent_to_be_appended_at_the_end = i
			#Remove that from the list
			resume_intent_list.remove(i)
			#Append at the end of the list
			resume_intent_list.append(intent_to_be_appended_at_the_end)
			intent_exists = 1
			break
	#If intent is not present in the list add it
	if intent_exists == 0:
		resume_intent_list.append(intent_to_resume)
	return resume_intent_list

	
def replaceResumableList(sender_id=None,data=None):
	if sender_id is not None and data is not None:
		set_slot(sender_id,"resumable",data)
		return {"success":1,"message":"List Updated"}
	else:
		return {"success":0,"message":"Unable to update data"}


def getResumableList(sender_id=None):
	user_resume_data = get_multiple_slot(sender_id)
	print(user_resume_data)
	if user_resume_data is not None:
		if "resumable" in user_resume_data and len(user_resume_data["resumable"]) >= 0:
			return {"success":1,"data":user_resume_data["resumable"]}
		else:
			print("in else of resume")
			return {"success":0,"data":{}}
	else:
		return {"success":0,"data":{}}


def addResumableListIfNotAddedInDB(sender_id=None,data=None):
	set_multiple_slots(sender_id,{"resumable":data})
	return {"success":1,"data":data}

def resumeForm(sender_id=None,current_form_name=None):
	#Get the resume intent list from Database
	resume_list_response = getResumableList(sender_id=sender_id)

	if resume_list_response.get("success") == 1:
		resume_intent_list = resume_list_response.get("data")
	else:
		resume_intent_list = []
		addResumableListIfNotAddedInDB(sender_id=sender_id,data=resume_intent_list)

	#If the list isn't none
	intent_to_resume = None
	if resume_intent_list:
		for i in resume_intent_list:
			#remove the current form from the resume list
			if i['form'] == current_form_name:
				resume_intent_list.pop()
				break
		
		#Check if the latest intent in the resume list is resumable or not
		i = 1
		while len(resume_intent_list) > 0:
			intent_to_resume = resume_intent_list[-i]
			if intent_to_resume.get("resumable") == 'yes':
				break
			else:
				resume_intent_list.pop()
				i = 1
		logging.info("above if")
		if resume_intent_list:
			# If there is something to resume
			print("there is something to resume")
			resume_confirmation = None
			intent_to_resume = resume_intent_list[-1]["intent_name"]
		else:
			#If there is nothing to resume
			resume_confirmation = "answered"
	else:
		#If there is nothing to resume
		resume_confirmation = "answered"
	replaceResumableList(sender_id=sender_id,data=resume_intent_list)
	return {"success":1,"resume_confirmation":resume_confirmation,"intent_to_resume":intent_to_resume}

def clearSlot(sender_id=None,intent_name=None,data={},response_message=None):
	#Static file that contains intent action mapping
	with open('./lib/intent_details.json', 'r') as f:
		message = json.load(f)
	#Get the intent details from json
	intent_details = message[intent_name]
	slots = []
	#To Clear the slots saved in DB.
	db_slots = {}
	if intent_details != None:
		intent_slots = intent_details.get("slots")
		for i in intent_slots:
			slots.append(SlotSet(i,None))
			db_slots[i] = None
	db_slots["resume_confirmation"] = data.get("resume_confirmation")
	db_slots["response_message"] = response_message
	db_slots["intent_to_resume"] = data.get("intent_to_resume")
	if data.get("intent_to_resume") != None:
		intent_details = message[data.get("intent_to_resume")]
		db_slots["resume_message"] = intent_details.get("resume_message")
	else:
		db_slots["resume_message"] = None
	set_multiple_slots(sender_id,db_slots)
	return slots