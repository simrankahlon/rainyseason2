# -*- coding: utf-8 -*-
import argparse
import logging

from sanic import Sanic, response
import json

from rasa.shared.core.domain import Domain
from rasa.core.nlg import TemplatedNaturalLanguageGenerator
from rasa.shared.core.trackers import DialogueStateTracker
import logging
from lib.db_queries import *
logger = logging.getLogger(__name__)

DEFAULT_SERVER_PORT = 5066

DEFAULT_SANIC_WORKERS = 1

def create_argument_parser():
	"""Parse all the command line arguments for the nlg server script."""

	parser = argparse.ArgumentParser(description="starts the nlg endpoint")
	parser.add_argument(
		"-p",
		"--port",
		default=DEFAULT_SERVER_PORT,
		type=int,
		help="port to run the server at",
	)
	parser.add_argument(
		"--workers",
		default=DEFAULT_SANIC_WORKERS,
		type=int,
		help="Number of processes to spin up",
	)
	parser.add_argument(
		"-d",
		"--domain",
		type=str,
		default=None,
		help="path of the domain file to load utterances from",
	)

	return parser

async def generate_response(nlg_call, domain):
	"""Mock response generator.
	Generates the responses from the bot's domain file.
	"""
	kwargs = nlg_call.get("arguments", {})
	template = nlg_call.get("template")
	logging.info("TEMPLATE: "+template)
	if template == "utter_default":
		slots = nlg_call.get("tracker",{}).get("slots")
		print(slots)
		if slots["requested_slot"] is not None:
			template = "utter_ask_"+slots["requested_slot"]
	sender_id = nlg_call.get("tracker", {}).get("sender_id")
	events = nlg_call.get("tracker", {}).get("events")
	tracker = DialogueStateTracker.from_dict(sender_id, events, domain.slots)
	channel_name = nlg_call.get("channel")

	return await TemplatedNaturalLanguageGenerator(domain.templates).generate(
		template, tracker, channel_name, **kwargs
	)


def run_server(domain, port, workers):
	app = Sanic(__name__)

	@app.route("/nlg", methods=["POST", "OPTIONS"])
	async def nlg(request):
		template = {}
		try:
			"""Endpoint which processes the Core request for a bot response."""
			nlg_call = request.json
			template = nlg_call.get("template")
			sender_id = nlg_call.get("tracker", {}).get("sender_id")
			slots = nlg_call.get("tracker",{}).get("slots")
			buttons = []
			if template == "utter_ask_resume_confirmation" or slots["requested_slot"] == "resume_confirmation":
				response_message = get_slot(sender_id,"response_message")
				if response_message == None:
					response_message = ""
				resume_message = get_slot(sender_id,"resume_message")
				text = str(response_message) + str(resume_message)
			elif template == "utter_ask_switch_confirmation" or slots["requested_slot"] == "switch_confirmation":
				current_intent = get_slot(sender_id,"current_intent")
				previous_intent = get_slot(sender_id,"previous_intent")
				text = "Would you like to switch from " + str(previous_intent) + " to " + str(current_intent) + " ?"
			else:
				bot_response = await generate_response(nlg_call, domain)
				text = None
				if "text" in bot_response:
					text = bot_response["text"]
				if "custom" in bot_response:
					text = bot_response["custom"]["text"] 
					print(bot_response["custom"])
					if "buttons" in bot_response["custom"]:
						for item in bot_response["custom"]["buttons"]:
							buttons.append({"title":" " + item,"payload":item,"type":"postback"})
			template = {"text":json.dumps(
				[
					{ 
						"message" : 
						{ 
							"template" : 
							{ 
								"elements" :
								{ 
									"title" : text, 
									"says": "",
									"visemes" : "",
									"buttons": buttons,
								},
								"type" : "Card"
							}
						}
					},
					{ 
						"ssml" : "<speak><s>"+text+"</s></speak>"
					}
				]
			).replace("\\\\n","\\n")}
			return response.json(template)
		except Exception as e:
			logging.exception(e)
			template = {"text":json.dumps(
				[
					{ 
						"message" : 
						{ 
							"template" : { 
								"elements" : {
									"title" : "I am sorry! I am still learning and I could not understand what you said. Please say that again.", 
									"says": "",
									"visemes" : ""
								},
								"type" : "Card"
							}
						}
					},
					{ 
						"ssml" : "<speak><s>I am sorry! I am still learning and I could not understand what you said. Please say that again.</s></speak>"
					}
				]
			)}
			return response.json(template)
	app.run(host="0.0.0.0", port=port, workers=workers)


if __name__ == "__main__":
	logging.basicConfig(level=logging.DEBUG)

	# Running as standalone python application
	arg_parser = create_argument_parser()
	cmdline_args = arg_parser.parse_args()
	_domain = Domain.load(cmdline_args.domain)

	run_server(_domain, cmdline_args.port, cmdline_args.workers)